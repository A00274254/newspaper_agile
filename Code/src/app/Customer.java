package app;

public class Customer {

    private int customerId;
    private String firstName;
    private String lastName;
    private int customer_id;

    public Customer(int studentId, String firstName, String lastName, int department_id) {
        super();
        this.customer_id = studentId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.customer_id = department_id;
    }

    public int getStudentId() {
        return customerId;
    }

    public void setStudentId(int studentId) {
        this.customerId = studentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getDepartment_id() {
        return customer_id;
    }

    public void setDepartment_id(int department_id) {
        this.customer_id = department_id;
    }

    @Override
    public String toString() {
        return "Student [studentId=" + customerId + ", firstName=" + firstName + ", lastName=" + lastName
                + ", department_id=" + customer_id + "]";
    }
}
