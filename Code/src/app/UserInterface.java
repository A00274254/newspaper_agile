package app;

import java.util.List;
import java.util.Scanner;

public class UserInterface {

    public static void main(String args[]) {

        NewspaperDOA database = new NewspaperDOA(); //Create a new instance of the database class

        Scanner in = new Scanner(System.in);
        int choice;

        do {
            System.out.println("\nEnter 1 to view all Customers");
            System.out.println("Enter 2 to view a particular customer");
            System.out.print("Enter a choice (-1 to quit): ");
            choice = in.nextInt();

            if(choice != -1)
            {
                if(choice == 1)
                {
                    List<Customer> customerList = database.getCustomers();
                    for (Customer customer: customerList)
                        System.out.println(customer.toString());
                }
                else if(choice == 2)
                {
                    System.out.print("Enter the student id of the customer you'd like to view: ");
                    int customerID = in.nextInt();
                    Customer stud = database.getCustomer(customerID);
                    System.out.println(stud);
                }
                else
                {
                    System.out.println("You entered an incorrect number!");
                }

            }
        }while(choice != -1);

        System.out.println("Goodbye!");
        in.close();
    }
}