package app;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class NewspaperDOA {

    public static Connection con;
    public static Statement stmt;

    public NewspaperDOA()
    {
        //Constructor does the connection to the database
        try
        {
            Class.forName("com.mysql.cj.jdbc.Driver");
            String url="jdbc:mysql://localhost:3306/college?useTimezone=true&serverTimezone=UTC";
            con = DriverManager.getConnection(url, "root", "admin");
            stmt = con.createStatement();
        }
        catch(Exception e)
        {
            System.out.println("Error: Failed to connect to database\n" + e.getMessage());
        }
    }

    //Method returns a Student object from the database
    public Customer getCustomer(int id)
    {
        Customer customer = null;
        try{
            ResultSet rs = stmt.executeQuery("SELECT * FROM student where student_id = "+ id);

            while (rs.next()) {
                int customerId = rs.getInt(1);
                String firstname = rs.getString(2);
                String lastname = rs.getString(3);
                int deliveryAreaId = rs.getInt(4);

                customer = new Customer(customerId, firstname, lastname, deliveryAreaId);
            }
        }
        catch(Exception e)
        {
            System.out.println("Error getting customer");
            e.printStackTrace();
        }
        return customer;
    }

    //Method returns a list of Customer objects from the database
    public List<Customer> getCustomers()
    {
        List<Customer> customerList = new ArrayList<Customer>();
        try{
            ResultSet rs = stmt.executeQuery("SELECT * FROM customer");

            while (rs.next()) {
                int customerId = rs.getInt(1);
                String firstname = rs.getString(2);
                String lastname = rs.getString(3);
                int delAreaId = rs.getInt(4);

                Customer customer = new Customer(customerId, firstname, lastname, delAreaId);
                customerList.add(customer);
            }
        }
        catch(Exception e)
        {
            System.out.println("Error getting Customers");
        }
        return customerList;
    }
}

