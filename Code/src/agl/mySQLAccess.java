package agl;

import java.sql.*;


public class mySQLAccess {

    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;

    final private String host ="localhost:3306";
    final private String user = "root";
    final private String password = "admin";


    public mySQLAccess() throws Exception {

        try {

            //Load MySQL Driver
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Setup the connection with the DB
            connect = DriverManager.getConnection("jdbc:mysql://" + host + "/newsagent2021?" + "user=" + user + "&password=" + password);
        }
        catch (Exception e) {
            throw e;
        }


    }

    public boolean insertDeliveryAreaDetailsAccount(DeliveryArea c) {

        boolean insertSucessfull = true;

        //Add Code here to call embedded SQL to insert Customer into DB

        try {

            //Create prepared statement to issue SQL query to the database
            preparedStatement = connect.prepareStatement("insert into newsagent2021.delivery area values (default, ?, ?, ?)");
            preparedStatement.setString(1, c.getName());
            preparedStatement.setString(2, c.getAddress());
            preparedStatement.executeUpdate();


        }
        catch (Exception e) {
            insertSucessfull = false;
        }

        return insertSucessfull;

    }

    public ResultSet retrieveAllDeliveryAreaAccounts() {


        try {
            statement = connect.createStatement();
            resultSet = statement.executeQuery("Select * from newsagent2021.customer");

        }
        catch (Exception e) {
            resultSet = null;
        }
        return resultSet;
    }

    public boolean deletedelAreaId(int delAreaID) {

        boolean deleteSucessfull = true;



        try {

            if (delAreaID == -99)
                preparedStatement = connect.prepareStatement("delete from newsagent2021.customer");
            else
                preparedStatement = connect.prepareStatement("delete from newsagent2021.customer where id = " + delAreaID);
            preparedStatement.executeUpdate();

        }
        catch (Exception e) {
            deleteSucessfull = false;
        }

        return deleteSucessfull;

    }
}


