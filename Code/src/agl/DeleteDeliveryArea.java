package agl;

public class DeleteDeliveryArea{
    private int id;
    private String name;
    private String address;

    public DeleteDeliveryArea(String delAreaName, String delAreaAddr) {
    }

    void setId(int delAreaId) {
        id = delAreaId;
    }

    void setName(String delAreaName) {
        name = delAreaName;
    }

    void setAddress(String delAreaAddr) {
        address = delAreaAddr;
    }

    int getId() {
        return id;
    }

    String getName() {
        return name;
    }

    String getAddress() {
        return address;
    }

    public DeleteDeliveryArea(String delAreaId, String delAreaAddr, String delAreaName) throws DeleteDeliveryAreaExceptionHandler
    {
        id = 0;

        try
        {
            deleteName(delAreaName);
            deleteAddress(delAreaAddr);
        }
        catch (DeleteDeliveryAreaExceptionHandler c)
        {
            throw c;
        }

        name = delAreaName;
        address = delAreaAddr;
    }

    public static void deleteName(String delAreaName) throws DeleteDeliveryAreaExceptionHandler
    {
        if (delAreaName.isBlank() || delAreaName.isEmpty())
            throw new DeleteDeliveryAreaExceptionHandler("Delivery Area Name NOT specified");
        else if (delAreaName.length() < 2)
            throw new DeleteDeliveryAreaExceptionHandler("Delivery Area Name does not meet minimum length requirements");
        else if (delAreaName.length() > 50)
            throw new DeleteDeliveryAreaExceptionHandler("Delivery Area Name does not exceeds maximum length requirements");
    }

    public static void deleteAddress(String delAreaAddr) throws DeleteDeliveryAreaExceptionHandler
    {
        if (delAreaAddr.isBlank() || delAreaAddr.isEmpty())
            throw new DeleteDeliveryAreaExceptionHandler("Delivery Area Address NOT specified");
        else if (delAreaAddr.length() < 5)
            throw new DeleteDeliveryAreaExceptionHandler("Delivery Area Address does not meet minimum length requirements");
        else if (delAreaAddr.length() > 60)
            throw new DeleteDeliveryAreaExceptionHandler("Delivery Area Address does not exceeds maximum length requirements");
    }
}
