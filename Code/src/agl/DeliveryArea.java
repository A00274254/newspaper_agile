package agl;

public class DeliveryArea {

    private int id;
    private String name;
    private String address;

    void setId(int delAreaId) {
        id = delAreaId;
    }

    void setName(String delAreaName) {
        name = delAreaName;
    }

    void setAddress(String delAreaAddr) {
        address = delAreaAddr;
    }


    int getId() {
        return id;
    }

    String getName() {
        return name;
    }

    String getAddress() {
        return address;
    }


    public DeliveryArea(String delAreaName, String delAreaAddr) throws DeliveryAreaExceptionHandler {

        id = 0;

        try {

            validateName(delAreaName);
            validateAddress(delAreaAddr);

        }
        catch (DeliveryAreaExceptionHandler e) {
            throw e;
        }

        // Set Attributes
        name = delAreaName;
        address = delAreaAddr;
    }

    public static void validateName(String delAreaName) throws DeliveryAreaExceptionHandler {
        if (delAreaName.isBlank() || delAreaName.isEmpty())
            throw new DeliveryAreaExceptionHandler("Delivery Area Name NOT specified");
        else if (delAreaName.length() < 2)
            throw new DeliveryAreaExceptionHandler("Delivery Area Name does not meet minimum length requirements");
        else if (delAreaName.length() > 50)
            throw new DeliveryAreaExceptionHandler("Delivery Area Name does not exceeds maximum length requirements");
    }

    public static void validateAddress(String delAreaAddr) throws DeliveryAreaExceptionHandler {
        if (delAreaAddr.isBlank() || delAreaAddr.isEmpty())
            throw new DeliveryAreaExceptionHandler("Delivery Area Address NOT specified");
        else if (delAreaAddr.length() < 5)
            throw new DeliveryAreaExceptionHandler("Delivery Area Address does not meet minimum length requirements");
        else if (delAreaAddr.length() > 60)
            throw new DeliveryAreaExceptionHandler("Delivery Area Address does not exceeds maximum length requirements");
    }


    // Deleting a Delivery Area

   /*  public void DeleteDeliveryArea(String delAreaName, String delAreaAddr) throws DeleteDelivereyAreaExceptionHandler{

        id = 2;

        try {

            deleteName(delAreaName);
            deleteAddress(delAreaAddr);

        }
        catch (DeleteDelivereyAreaExceptionHandler e) {
            throw e;
        }

        // Set Attributes
        name = delAreaName;
        address = delAreaAddr;
    }

    public static void deleteName(String delAreaName) throws DeleteDelivereyAreaExceptionHandler
    {
        if (delAreaName.isBlank() || delAreaName.isEmpty())
            throw new DeleteDelivereyAreaExceptionHandler("Delivery Area Name NOT specified");
        else if (delAreaName.length() < 2)
            throw new DeleteDelivereyAreaExceptionHandler("Delivery Area Name does not meet minimum length requirements");
        else if (delAreaName.length() > 50)
            throw new DeleteDelivereyAreaExceptionHandler("Customer Name does not exceeds maximum length requirements");
    }

    public static void deleteAddress(String delAreaAddr) throws DeleteDelivereyAreaExceptionHandler
    {
        if (delAreaAddr.isBlank() || delAreaAddr.isEmpty())
            throw new DeleteDelivereyAreaExceptionHandler("Delivery Area Address NOT specified");
        else if (delAreaAddr.length() < 5)
            throw new DeleteDelivereyAreaExceptionHandler("Delivery Area Address does not meet minimum length requirements");
        else if (delAreaAddr.length() > 60)
            throw new DeleteDelivereyAreaExceptionHandler("Delivery Area Address does not exceeds maximum length requirements");
    } */
}
