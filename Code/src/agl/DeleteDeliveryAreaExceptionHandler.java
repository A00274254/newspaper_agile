package agl;

public class DeleteDeliveryAreaExceptionHandler extends Exception
{
    String message;

    public DeleteDeliveryAreaExceptionHandler(String errMessage){
        message = errMessage;
    }

    public String getMessage() {
        return message;
    }
}
