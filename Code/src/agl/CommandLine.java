package agl;

import java.sql.ResultSet;
import java.util.Scanner;

public class CommandLine {

    private static void listDeliveryAreaFunctionalityAvailable() {



        System.out.println(" ");
        System.out.println("=============================================");
        System.out.println("Please choose ONE of the following options:");
        System.out.println("1. Create Delivery Area");
        System.out.println("2. View ALL Delivery Area Records");
        System.out.println("3. Delete Delivery Area Record by ID");
        System.out.println("99. Close the NewsAgent Application");
        System.out.println("=============================================");
        System.out.println(" ");

    }


    private static boolean printDeliveryAreaTable(ResultSet rs) throws Exception {

        //Print The Contents of the Full Customer Table

        System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("Table: " + rs.getMetaData().getTableName(1));
        for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
            System.out.printf("%30s",rs.getMetaData().getColumnName(i));
        }
        System.out.println();
        while (rs.next()) {
            int id = rs.getInt("id");
            String name = rs.getString("delAreaName");
            String addr = rs.getString("delAreaAddr");
            System.out.printf("%30s", id);
            System.out.printf("%30s", name);
            System.out.printf("%30s", addr);
            System.out.println();
        }// end while
        System.out.println("--------------------------------------------------------------------------------------------------------------------------------------");

        return true;

    }

    public static void main(String[] args) {

        try {

            mySQLAccess dao = new mySQLAccess();


            Scanner keyboard = new Scanner(System.in);
            String functionNumber = "-99";
            boolean keepAppOpen = true;

            while (keepAppOpen == true) {


                listDeliveryAreaFunctionalityAvailable();
                functionNumber = keyboard.next();

                switch (functionNumber) {

                    case "1":
                        System.out.printf("Enter Delivery Area Name: \n");
                        String delAreaName = keyboard.next();
                        System.out.printf("Enter Delivery Area Address: \n");
                        String delAreaAddr = keyboard.next();

                        DeliveryArea delAreaObj = new DeliveryArea(delAreaName,delAreaAddr);

                        boolean insertResult = dao.insertDeliveryAreaDetailsAccount(delAreaObj);
                        if (insertResult == true)
                            System.out.println("Delivery Area Details Saved");
                        else
                            System.out.println("ERROR: Delivery Area Details NOT Saved");
                        break;

                    case "2":

                        ResultSet rSet = dao.retrieveAllDeliveryAreaAccounts();
                        if (rSet == null) {
                            System.out.println("No Records Found");
                            break;
                        }
                        else {
                            boolean tablePrinted = printDeliveryAreaTable(rSet);
                            if (tablePrinted == true)
                                rSet.close();
                        }
                        break;

                    case "3":
                        System.out.println("Enter Delivery Area Id to be deleted or -99 to Clear all Rows");
                        String deletedelAreaId = keyboard.next();
                        boolean deleteResult = dao.deletedelAreaId(Integer.parseInt(deletedelAreaId));
                        if ((deleteResult == true) && (deletedelAreaId.equals("-99")))
                            System.out.println("Delivery Area Table Emptied");
                        else if (deleteResult == true)
                            System.out.println("Delivery Area Deleted");
                        else
                            System.out.println("ERROR: Delivery Area Details NOT Deleted or Do Not Exist");
                        break;

                    case "99":
                        keepAppOpen = false;
                        System.out.println("Closing the Application");
                        break;

                    default:
                        System.out.println("No Valid Function Selected");
                        break;
                }

            }

            keyboard.close();
        }

        catch(Exception e) {
            System.out.println("PROGRAM TERMINATED - ERROR MESSAGE:" + e.getMessage());
        }
    }
}
