package agl;

import junit.framework.TestCase;

public class DeliveryAreaTest extends TestCase {

    //Test #: 1
    //Test Objective: To create a Delivery Area
    //Inputs: delAreaName = "Delivery Area 1", delAreaAddr = "Boyne Heights"
    //Expected Output: Delivery Area Object created with id = 0, "Delivery Area 1", delAreaAddr = "Boyne Heights"

    public void testDeliveryArea001(){
        try
        {
        DeliveryArea delAreaObj = new DeliveryArea("Delivery Area 1", "Boyne Heights");

        assertEquals(0,delAreaObj.getId());
        assertEquals("Delivery Area 1", delAreaObj.getName());
        assertEquals("Boyne Heights", delAreaObj.getAddress());
        }

        catch (DeliveryAreaExceptionHandler e){
            fail("Exception not expected");
        }
    }
/*
    //Test #: 2
    //Test Objective: Assign a delivery person to a delivery area
    //Inputs: delAreaPerson = "Liam Downing" delAreaName = "Delivery Area 4", delAreaAddr = "Willow Park"
    //Expected Output: Delivery Area Person assigned to Delivery Area Object created with id = 5, "Delivery Area 3", delAreaAddr = "Willow Park"

    public void testDeliveryArea002()
    {
        try{
            DeliveryAreaPerson delAreaObj = new DeliveryArea("")
        }
    }  */
}
