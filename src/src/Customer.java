public class Customer
{
    private int id;
    private String firstName;
    private String surname;
    private String address;
    private String phoneNumber;

    void setId(int customerId)
    {
        id = customerId;
    }

    void SetFirstName(String customerFirstName)
    {
        firstName = customerFirstName;
    }

    void SetSurname(String customerSurname)
    {
        surname = customerSurname;
    }

    void SetAddress(String customerAddress)
    {
        address = customerAddress;
    }

    void SetPhoneNumber(String customerPhoneNumber)
    {
        phoneNumber = customerPhoneNumber;
    }

    int getId()
    {
        return id;
    }

    String getFirstName()
    {
        return firstName;
    }

    String getSurname()
    {
        return surname;
    }

    String getAddress()
    {
        return address;
    }

    String getPhoneNumber()
    {
        return phoneNumber;
    }

    public Customer(String customerFirstName, String customerSurname, String customerAddress, String customerPhoneNumber) throws CustomerExceptionHandler
    {
        id = 0;

        // Validate Input
        try
        {
            validateFirstName(customerFirstName);
            validateSurname(customerSurname);
            validateAddress(customerAddress);
            validatePhoneNumber(customerPhoneNumber);
        }

        catch (CustomerExceptionHandler e)
        {
            throw e;
        }

        // Set Attributes
        firstName = customerFirstName;
        surname = customerSurname;
        address = customerAddress;
        phoneNumber = customerPhoneNumber;
    }

    public static void validateFirstName(String customerFirstName) throws CustomerExceptionHandler
    {
        if (customerFirstName.isEmpty())
            throw new CustomerExceptionHandler("Customer Name NOT specified");
        else if (customerFirstName.length() < 2)
            throw new CustomerExceptionHandler("Customer Name does not meet minimum length requirements");
        else if (customerFirstName.length() > 50)
            throw new CustomerExceptionHandler("Customer Name exceeds maximum length requirements");
    }

    public static void validateSurname(String customerSurname) throws CustomerExceptionHandler
    {
        if (customerSurname.isEmpty())
            throw new CustomerExceptionHandler("Customer Name NOT specified");
        else if (customerSurname.length() < 2)
            throw new CustomerExceptionHandler("Customer Name does not meet minimum length requirements");
        else if (customerSurname.length() > 50)
            throw new CustomerExceptionHandler("Customer Name exceeds maximum length requirements");
    }

    public static void validateAddress(String customerAddress) throws CustomerExceptionHandler
    {
        if (customerAddress.isEmpty())
            throw new CustomerExceptionHandler("Customer Address NOT specified");
        else if (customerAddress.length() < 5)
            throw new CustomerExceptionHandler("Customer Address does not meet minimum length requirements");
        else if (customerAddress.length() > 60)
            throw new CustomerExceptionHandler("Customer Address exceeds maximum length requirements");
    }

    public static void validatePhoneNumber(String customerPhoneNumber) throws CustomerExceptionHandler
    {
        if (customerPhoneNumber.isEmpty())
            throw new CustomerExceptionHandler("Customer PhoneNumber NOT specified");
        else if (customerPhoneNumber.length() < 7)
            throw new CustomerExceptionHandler("Customer PhoneNumber does not meet minimum length requirements");
        else if (customerPhoneNumber.length() > 15)
            throw new CustomerExceptionHandler("Customer PhoneNumber exceeds maximum length requirements");
    }


}
