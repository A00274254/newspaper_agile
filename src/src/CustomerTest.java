import junit.framework.TestCase;

public class CustomerTest extends TestCase {

    //Test #: 1
    //Test Objective: To create a Customer Account
    //Inputs: customerFirstName = "Jack", customerSurname "Smith",  customerAddress = "Athlone", customerPhoneNumber = "087-123123123"
    //Expected Output: Customer Object created with id = 0, "Jack", customerSurname "Smith",  customerAddress = "Athlone", customerPhoneNumber = "087-123123123"

    public void testCustomer001() {

        //Create the Customer Object
        try {

            //Call method under test
            Customer customerObj = new Customer("Jack", "Smith", "Athlone", "087-123123123");

            // Use getters to check for object creation
            assertEquals(0, customerObj.getId());
            assertEquals("Jack", customerObj.getFirstName());
            assertEquals("Smith", customerObj.getSurname());
            assertEquals("Athlone", customerObj.getAddress());
            assertEquals("087-123123123", customerObj.getPhoneNumber());
        } catch (CustomerExceptionHandler e) {
            fail("Exception not expected");
        }

    }

    public static void assertEquals(String jack, String firstName) {
    }

    public static void assertEquals(int i, int id) {
    }

    //Test #: 2
    //Test Objective: To catch an invalid customer name
    //Inputs: customerFirstName = "J"
    //Expected Output: Exception Message: "Customer Name does not meet minimum length requirements"

    public void testValidateName002()
    {
        try {
            //Call method under test
            Customer.validateFirstName("J");
            fail("Exception expected");
        } catch (Exception exception) {
            fail("Customer Name does not meet minimum length requirements");
        }
    }

    //Test #: 3
    //Test Objective: To catch an invalid customer phone number
    //Inputs: customerFirstName = "087-123123"
    //Expected Output: Exception Message: "Customer phone number does not meet minimum length requirements"

    public void testValidateName003()
    {
        try {
            //Call method under test
            Customer.validatePhoneNumber("087-123123");
            fail("Exception expected");
        } catch (Exception exception) {
            fail("Customer phone number does not meet minimum length requirements");
        }
    }

    //Test #: 4
    //Test Objective: To catch an invalid customer address
    //Inputs: customerFirstName = "ath"
    //Expected Output: Exception Message: "Customer address does not meet minimum length requirements"

    public void testValidateName004()
    {
        try {
            //Call method under test
            Customer.validateAddress("ath");
            fail("Exception expected");
        } catch (Exception exception) {
            fail("Customer address does not meet minimum length requirements");
        }
    }

    //Test #: 5
    //Test Objective: To catch an invalid customer surname
    //Inputs: customerFirstName = "ti"
    //Expected Output: Exception Message: "Customer surname does not meet minimum length requirements"

    public void testValidateName005()
    {
        try {
            //Call method under test
            Customer.validateSurname("ti");
            fail("Exception expected");
        } catch (Exception exception) {
            fail("Customer surname does not meet minimum length requirements");
        }
    }
    public static void fail(String exception_expected) {
    }
}